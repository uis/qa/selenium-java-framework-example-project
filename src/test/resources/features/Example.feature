Feature: Google search

  Scenario: As a web user, I want to perform a Google search
    Given I go to Google
    When I enter the search term "Pineapple"
    Then The page title is "Pineapple - Google Search"
    And I see the link "Wikipedia" on the page